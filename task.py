import mysql.connector
from mysql.connector import Error

from connector import create_connection, execute_read_query
from registration import execute_write_query


def execute_task(connection, query):
    flag_task = False
    try:
        execute_write_query(connection, query)
        flag_task = True
    except Error as e:
        print(f"The error '{e}' occurred")
        
    return flag_task


def main():
    connection = create_connection("localhost", "chain", "frankfurt", "chain")

    task_data = [i.rstrip() for i in open("task_data.txt")]
    
    create_task = f"""
    INSERT INTO
        `tasks` (`email`, `description`, `text`)
    VALUES
        ('{task_data[0]}', '{task_data[1]}', '{task_data[2]}');
    """
    
    flag_task = execute_task(connection, create_task)


if __name__ == '__main__':
    main()
