import mysql.connector
from mysql.connector import Error

from connector import create_connection, execute_read_query


def login_user(connection, query):
    flag_login = False
    flag_role = None

    try:
        user = execute_read_query(connection, query)
        flag_login = True
        flag_role = user[2]
    except Error as e:
        print(f"The error '{e}' occurred")

    return flag_login, flag_role


def main():
    connection = create_connection("localhost", "chain", "frankfurt", "chain")

    login_data = [i.rstrip() for i in open("login_data.txt")]

    select_user = f"SELECT email, password, role FROM users WHERE email=:{login_data[0]} and pass=:{login_data[1]}"
    flag_login, flag_role = login_user(connector.connection, select_user)


if __name__ == '__main__':
    main()
