# A chain of good deeds
This is web platform to maintain people with disabilities with the help of volunteers.

## Presentation
[Presentation on google drive](https://docs.google.com/presentation/d/1knNomH2lvQN1TygHs4UEVm4KGb0zp9P_TCHlI-k4JTc/edit#slide=id.gbc27745ea6_0_15)

## Website
[A chain of good deeds](http://3.120.98.12/index.html)

## Developers
- [Alisher Yuldashov](https://github.com/fuckinrobotics)
- [Danil Matveev](https://gitlab.com/dsmatveev_1)
- [Polina Podkopaeva](https://gitlab.com/pollyaccord)
- [Ilya Mikhaylov](https://gitlab.com/iamikhaylov)
