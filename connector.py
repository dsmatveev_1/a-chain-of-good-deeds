import mysql.connector
from mysql.connector import Error


def execute_read_query(connection, query):
    cursor = connection.cursor()
    result = None

    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as e:
        print(f"The error '{e}' occurred")


def create_connection(host_name, user_name, user_password, db_name):
    connection = None

    try:
        connection = mysql.connector.connect(host=host_name,
                                             user=user_name,
                                             passwd=user_password,
                                             database=db_name)
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


def main():
    connection = create_connection("localhost", "chain", "frankfurt", "chain")


if __name__ == '__main__':
    main()
