import mysql.connector
from mysql.connector import Error

from connector import create_connection, execute_read_query


def execute_write_query(connection, query):
    cursor = connection.cursor()
     
    try:
        cursor.execute(query)
        connection.commit()
    except Error as e:
        print(f"The error '{e}' occurred")


def registrate_user(connection, query, email):
    flag_registration = False
    
    try:
        user_email = execute_read_query(connection, "SELECT email FROM users WHERE email=:{reg_data[1]}")
    except Error:
        try:
            execute_write_query(connection, query)
            flag_registration = True
        except Error as e:
            print(f"The error '{e}' occurred")
        
    return flag_registration


def main():
    connection = create_connection("localhost", "chain", "frankfurt", "chain")
	
    reg_data = [i.rstrip() for i in open("reg_data.txt")]
    
    create_user = f"""
    INSERT INTO
        `users` (`name`, `email`, `password`)
    VALUES
        ('{reg_data[0]}', '{reg_data[1]}', '{reg_data[2]}');
    """
    
    flag_registration = registrate_user(connection, create_user, reg_data[1])


if __name__ == '__main__':
    main()
